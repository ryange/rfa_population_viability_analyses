# RFA_population_viability_analyses

Population viability analyses and metapopulation capacity analyses for biodiversity in the Central Highlands and East Gippsland Regional Forestry Agreement areas in Victoria, Australia.